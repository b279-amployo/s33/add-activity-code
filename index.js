// MAP

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));




// TO-DO list GET method

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});




// TO-DO list

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));





// [TO-DO list using POST

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a to do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a to do list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	status: 'Complete',
	  	dateCompleted: '07/09/21'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});